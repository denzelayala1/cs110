import java.util.Scanner;
import java.io.*;

public class ArrayFunctions{
     /*
    writeArray – takes in a String representing the filename, an array of Strings and returns a boolean.
The method will write the names to the file, preceded by numbers.
1: Ana
2: Aria
…
The return value indicates if the write was successful (true) or not (false);
Exception handling: this method will have no throws clause – you must use exception handling to handle the
following exceptions.
 If the array reference is null and you attempt to access the array, a NullPointerException
will be thrown. If this happens, return false.
 If any file exception occurs, return false. 
    */

    public static int readArray(String fileName, String[] nameArray){

        int numberOfNames = 0;


        try{

            File file = new File(fileName);
            Scanner scanFile = new Scanner(file);

            while(numberOfNames < nameArray.length && scanFile.hasNextLine()){
                nameArray[numberOfNames] = scanFile.nextLine();
                numberOfNames += 1;
            }

            scanFile.close();
        }
        catch(FileNotFoundException e){
            System.out.println("The file was not found");
            return 0; 
        }
        catch(NullPointerException e){
            System.out.println("The array appears to be empty");
            return 0;
        }

        return numberOfNames;
    }

    public static boolean writeArray(String fileName, String[] nameArray){

        int count  = 0;
        try{

            FileWriter outFile = new FileWriter(fileName);
            PrintWriter printToFile = new PrintWriter(outFile);

            while(count < nameArray.length){
                printToFile.printf( (count + 1) + ": " + nameArray[count] + "\n" );
                count += 1;
            }

            printToFile.close();
        }
        catch(NullPointerException e){
            System.out.println("The array is empty");
            return false;
        }
        catch(IOException e){
            System.out.println("There is an error with the file");
            return false;
        }
        
        return true;
    }
    
    public static void selectionSort(String[] nameArray, int numberOfNames){

        int startIndex, currentIndex, minIndex;
        String alphabeticalName;

        for(startIndex = 0; startIndex < numberOfNames - 1; startIndex++){

            minIndex = startIndex;
            alphabeticalName = nameArray[startIndex];

            for(currentIndex = startIndex + 1; currentIndex < numberOfNames; currentIndex++){
                if(alphabeticalName.compareTo(nameArray[currentIndex]) > 0){
                    alphabeticalName = nameArray[currentIndex];
                    minIndex = currentIndex;
                }
            }

            nameArray[minIndex] = nameArray[startIndex];
            nameArray[startIndex] = alphabeticalName;
        }
    }

    public static int binarySearch(String[] sortedNameArray, int first, int last, String desiredName){

        if(first > last){
            return -1;
        }

        int middle;
        middle = (first + last) / 2;

        if( desiredName.compareToIgnoreCase(sortedNameArray[middle]) == 0){
            return middle;
        }
        else if( desiredName.compareToIgnoreCase(sortedNameArray[middle]) > 0){
            return ArrayFunctions.binarySearch(sortedNameArray, first, middle - 1 , desiredName);
        }
        else{
            return ArrayFunctions.binarySearch(sortedNameArray, middle + 1 , last, desiredName);
        }
    }

}