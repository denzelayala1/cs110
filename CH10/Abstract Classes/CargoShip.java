/*
   Denzel Ayala
   CS110

    This program describes the Cargo ship class that extends the ship
    a Cargo ships is defined by its name, the year it was made and
    the max tonnage capacity of the cargo ship
*/

public class CargoShip extends Ship{
    
    private int tonnage; //the max tonnage capacity of the cargo ship


    /**
     * Constructs and instence of a Cargo ship
     * @param name the cargo ship's name
     * @param year the year the cargo ship wasz made
     * @param tons the max tonnage capacity of the cargo ship
     */
    public CargoShip(String name, int year, int tons){
        super(name, year);
        tonnage = tons;
    }


    /**
     * Gets the max tonnage capacity of the cargo ship
     * @return the max tonnage capacity of the cargo ship
     */
    public int getTonnage(){
        return tonnage;
    }


    /**
     * Sets  the max tonnage capacity of the cargo ship
     * @param tonnage the max tonnage capacity of the cargo ship
     */
    public void setTonnage(int tonnage){
        this.tonnage = tonnage;
    }


    /**
     * A Cargo ship string prints out the ship's name and the max tonnage capacity of the cargo ship
     */
    @Override
    public String toString(){
        return "\nShip name: "+ this.getName() + "\nCargo Capacity: " + tonnage + " tons";
    }
}