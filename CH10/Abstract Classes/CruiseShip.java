/*
   Denzel Ayala
   CS110

    This program describes the Cruise ship class that extends the ship
    a Cruise ships is defined by its name, the year it was made and
    the number of passangers it can hold
*/

public class CruiseShip extends Ship{

    private int numPassangers; // the number of passangers the cruise ship can hold


    /**
     * Constructs an instence of a Cruise ship
     * @param name the name of the cruise ship
     * @param year the year the cruise ship was made
     * @param passangers the number of passangers the cruise ship can hold
     */
    public CruiseShip(String name, int year, int passangers){
        super(name,year); // call ship constructor
        numPassangers = passangers; 
    }


    /**
     * Gets the number of passangers the cruise ship can hold
     * @return the number of passangers the cruise ship can hold
     */
    public int getNumPassangers(){
        return numPassangers;
    }


    /**
     * Sets the number of passangers the cruise ship can hold
     * @param numPassangers the number of passangers the cruise ship can hold
     */
    public void setNumPassangers(int numPassangers){
        this.numPassangers = numPassangers;
    }


    /**
     * A Cruise ship string prints out the ship's name and the number of passangers the cruise ship can hold
     */
    @Override
    public String toString(){
        return "\nShip name: "+ this.getName() + "\nNumber of Passangers: " + numPassangers;
    }
}