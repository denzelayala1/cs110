import java.util.Scanner;

public class FileDriver{

    public static void main(String[] args) {
        
        String  fileName,
                nextInput = "This";

        int numOfNames = -1;
        Scanner keyboard = new Scanner(System.in);

        System.out.println("What is the name of the file you want to look through?");
        fileName = keyboard.nextLine();
    
        System.out.println("How many names would you like to look through?");

        while(numOfNames < 0){
            try{
                nextInput = keyboard.nextLine();
                numOfNames = Integer.parseInt(nextInput);
            }
            catch(NumberFormatException e){
                System.out.printf("%s is not an integer.\nEnter a positive integer value ", nextInput);
            }
        }

        String[] nameArray = new String[numOfNames];

        int namesInArray;
        namesInArray = ArrayFunctions.readArray(fileName, nameArray);
        if(namesInArray < numOfNames){
            System.out.printf("The file only contained %d names.\n", namesInArray);
        }
        
        /*
        for(int i = 0; i < namesInArray; i++){
            System.out.println(nameArray[i]);
        }
        */

        ArrayFunctions.selectionSort(nameArray, namesInArray);
        
        String outFileName = "sorted_" + fileName;

        if(!ArrayFunctions.writeArray(outFileName, nameArray) ){
            System.out.println("write was unsucessful");
        }

        String desiredName;
        
        System.out.println("What name would you like to look for?");
        desiredName = keyboard.nextLine();

        int locationOfDeiredName;
        locationOfDeiredName = ArrayFunctions.binarySearch(nameArray, 0, namesInArray - 1, desiredName);

        if( locationOfDeiredName >= 0 ){
            System.out.printf("%s was %d on the list\n", desiredName, locationOfDeiredName);
        }
        else{
            System.out.printf(Integer.toString(locationOfDeiredName) +"\n" );
            System.out.printf("%s was not in the top %d\n", desiredName, namesInArray);
        }

        System.out.println(outFileName+ "\n");

        /*
        for(int i = 0; i < namesInArray; i++){
            System.out.println(nameArray[i]);
        }
        */
        keyboard.close();
    }
    
}