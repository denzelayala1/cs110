import java.util.Random;

/*
   Denzel Ayala
   CS110

    This program extends the random class and creates a loaded die
*/

public class LoadedDice extends Random{


    /**
     * Overrides the default nextInt method to one that has a 50% chance of
     *  returning the highest possible value
     */
    @Override
    public int nextInt(int n){

        Random fiftyFifty = new Random(); // instantiates a random generator

        int load = fiftyFifty.nextInt(2); // rolls a 0 or 1

        //If load was 0 it will return the highest value of the die (M)
        // if its 1 then it will randomly return a value between 0 and M-1
        if(load == 0){
            return n-1;
        }
        else{
            return fiftyFifty.nextInt(n-2);
        }
        
    }
}