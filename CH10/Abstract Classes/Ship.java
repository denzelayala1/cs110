/*
   Denzel Ayala
   CS110

    This program describes the abstract Ship class
    a ship is defined by its name and the year it was made 
*/

 public abstract class Ship {

    private String name; // Ship name
    private int year; // Year ship was made

    /**
     * Constructor for a Ship
     * @param shipName The name of the ship 
     * @param shipYear The year the ship was made
     */
    public Ship(String shipName, int shipYear){
        name = shipName;
        year = shipYear;
    }


    //Getters
    /**
     * Returns the name of the ship 
     * @return the name of the ship
     */
    public String getName(){
        return name;
    }

    /**
     * Returns the year the ship was made
     * @return the year the ship was made
     */
    public int getYear(){
        return year;
    }


    //Setters
    /**
     * Sets the name of the ship
     * @param name the name of the ship
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Sets the year the ship was made
     * @param year the year the ship was made
     */
    public void setYear(int year){
        this.year = year;
    }

    /**
     * A ship string is defined by its name and its year
     */
    public String toString(){
        return "\nShip name: "+ name + "\nYear built: " + year;
    }

}