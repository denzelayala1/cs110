/*
Demonstrate the classes in a program that has a Ship ArrayList. Assign various CruiseShip and
CargoShip objects to the ArrayList elements. The program should then step through the ArrayList,
calling each object’s toString method.
*/
import java.util.ArrayList;


public class ShipDriver{

    public static void main(String[] args){

        Ship princess = new CruiseShip("Princess", 1994, 3000),
             carnival = new CruiseShip("Carnival", 1974, 2340),
             didney = new CruiseShip("Didney Wonder", 2003, 4300),
             bigBoy = new CargoShip("Big Boy", 1967, 25000),
             chonker =  new CargoShip("Chonker the Ship", 2000, 34000),
             largeChungus = new CargoShip("The Largest Chungus", 2020, 60000);

        

        ArrayList<Ship> shipList = new ArrayList<>();

        shipList.add(princess);
        shipList.add(carnival);
        shipList.add(didney);
        shipList.add(bigBoy);
        shipList.add(chonker);
        shipList.add(largeChungus);

        for(int i = 0; i < shipList.size(); i++){
            System.out.println(shipList.get(i).toString());
        }



    }
    
}