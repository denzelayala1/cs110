/*
   Denzel Ayala
   CS110

    This program defines the Person Class
    a person is defined by their name, address and their phone number
*/

public class Person{

    private String  name, // The name of the person
                    address, // the person's address
                    phone; // the person's phone number

    
    //Constructors
    /**
     * A constructor for a person object
     * @param nm A string representing the name of the person
     * @param adrs A string representing the person's address
     * @param phn A string representing the person's phone number
     */
    public Person(String nm, String adrs, String phn){

        name = nm; 
        address = adrs;
        phone = phn;

    }

    //Getters
    /**
     * Getter for the person's name
     * @return A string that represent the person's name
     */
    public String getName(){
        return name;
    }

    /**
     * Getter for the person's address
     * @return A string that represent the person's address
     */
    public String getAddress(){
        return address;
    }

    /**
     * Getter for the person's phone number
     * @return A string that represent the person's phone number
     */
    public String getPhone(){
        return phone;
    }


    //Setters
    /**
     * Setter for the person's name
     * @param name A string representing the person's name
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Setter for the person's address
     * @param address A string representing the person's address
     */
    public void setAddress(String address){
        this.address = address;
    }

    /**
     * Setter for the person's phone number
     * @param phone A string representing the person's phone number
     */
    public void setPhone(String phone){
        this.phone = phone;
    }

    //Additional Methods
    /**
     * converts the person object into a string object
     * @return A string that represent the critical information of a person object
     */
    public String toString(){
        return name + ", " + address + ", " + phone;
    }
    
    /**
     * Checks to see if two person objects are the same
     * @return Returns true if the two person objects have the same name, address and phone number
     */
    public boolean equals(Object obj2){

        Person p2 = (Person)obj2;

        if( this.name == p2.name &&
            this.address == p2.address &&
            this.phone == p2.phone){
                return true;
            }
        else{
            return false;
        }
    }
}