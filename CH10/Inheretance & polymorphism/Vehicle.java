/*
   Denzel Ayala
   CS110

    This program describes the Vehicle class
    a vehicle is defined by its owner which is a person object, its make, model, year and mileage
*/

public class Vehicle{
    
    private Person  owner; // person object to hold vehicle owner  
    private String  make, // make of the vehicle
                    model; // model of the vehicle
    private int     year, // year of the vehicle
                    mileage; // mileage of the vehicle


    //Constructors
    /**
     * This constructs an instance of the Vehicle object
     * @param own A person object that represents the owner of the vehicle
     * @param mk A string that is the make of the vehicle
     * @param mod A string that is the model of the vehicle
     * @param yr an integer that is the year the vehicle was made
     * @param mile an integer that is the milage on the vehicle
     */
    public Vehicle(Person own, String mk, String mod, int yr, int mile){

         owner = new Person(own.getName(), 
                                  own.getAddress(), 
                                  own.getPhone()); // deep copy of the owner
        
        make = mk;
        model = mod;
        year = yr;
        mileage = mile;
    }

    /**
     *  This constructor creates an instance of the Vehicle object with zero milage
     * @param own A person object that represents the owner of the vehicle
     * @param mk A string that is the make of the vehicle
     * @param mod A string that is the model of the vehicle
     * @param yr an integer that is the year the vehicle was made
     * @param mile an integer that is the milage on the vehicle
     */
    public Vehicle(Person own, String mk, String mod, int yr){
        this(own, mk, mod, yr, 0);
    }

    //Getters
    /**
     *  Getter for Owner
     * @return This returns a deep copy of the person object "owner"
     */
    public Person getOwner(){
        Person retPerson = new Person(this.owner.getName(), 
                                      this.owner.getAddress(),
                                      this.owner.getPhone());
        return retPerson;
    }

    /**
     * Getter for the make of the vehicle
     * @return A string that represent the make of the vehicle
     */
    public String getMake(){
        return make;
    }
    
    /**
     * Getter for the model of the vehicle
     * @return A string that represent the model of the vehicle
     */
    public String getModel(){
        return model;
    }

    /**
     * Getter for the year of the vehicle
     * @return A integer that represent the year of the vehicle
     */
    public int getYear(){
        return year;
    }

    /**
     * Getter for the mielage of the vehicle
     * @return A integer that represent the make of the vehicle
     */
    public int getMileage(){
        return mileage;
    }


    //Setters
    /**
     * Sets the owner of the vehicle
     * @param own A person object that will represent the owner of the vehicle
     */
    public void setOwner(Person own){
        owner = new Person(own.getName(),
                           own.getAddress(),
                           own.getPhone());
    }

    /**
     * Setter for the make of the vehicle
     * @param make A string that represent the make of the vehicle
     */
    public void setMake(String make){
        this.make = make;
    }

    /**
     * Setter for the model of the vehicle
     * @param make A string that represent the model of the vehicle
     */
    public void setModel(String model){
        this.model = model;
    }

    /**
     * Setter for the year of the vehicle
     * @param year An integer that represent the year of the vehicle
     */
    public void setYear(int year){
        this.year = year;
    }

    /**
     * Setter for the mileage of the vehicle
     * @param mileage An integer that represent the milage of the vehicle
     */
    public void setMilage(int mileage){
        this.mileage = mileage;
    }


    //Addtional methods
    /**
     * 
     * @return A string that contains the critical information of the vehicle
     */
    public String toString(){
        return this.owner.toString() + "\n" + 
                make + " " + model + " " + year + 
                " " + mileage + " miles";
    }

    /**
     * The equals method checks to see if two vehicle objects have the same properties
     * @return Returns True if the two objects share the same owner, make, model, year, and mileage
     */
    @Override
    public boolean equals(Object obj){

        Vehicle vh2 = (Vehicle)obj;

        if( this.owner.equals(vh2.owner) &&
            this.make == vh2.getMake() &&
            this.model == vh2.getModel() &&
            this.year == vh2.getYear() &&
            this.mileage == vh2.getMileage()){
            
                return true;                
            }
        else{
            return false;
        }

    }
}