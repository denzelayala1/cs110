public class mod8Tester{
    public static void main(String[] args){
        
        Taxi auto1 = new Taxi("Pat Hampton", "99 Second St, Burlington, VT 05401",
                                          "802-578-3987", "Dodge", 
                                          "Durango", 2015, 
                                          78888, 7, 
                                          true, "Pat Hampton", "99 Second St, Burlington, VT 05401",
                                          "802-578-3987", "34F98G4HA");

        Taxi auto2 = new Taxi("Pat Hampton", "99 Second St, Burlington, VT 05401",
                                          "802-578-3987", "Dodge", 
                                          "Durango", 2015, 
                                          78888, 7, 
                                          true, "Pat Hampton", "99 Second St, Burlington, VT 05401",
                                          "802-578-3987", "34F98G4HA");

        Taxi auto3 = new Taxi("Pat Hampton", "99 Second St, Burlington, VT 05401",
                                        "802-578-3987", "Dodge", 
                                        "Durango", 2015, 
                                        78888, 5, 
                                        false, "Pat Hampton", "99 Second St, Burlington, VT 05401",
                                        "802-578-3987", "34F98G4HA");
                            
        if(auto1.equals(auto2)){
            System.out.println("these are the same");
        }
        else{
            System.out.println("somethings wrong");
        }

        if(!auto1.equals(auto3)){
            System.out.println("these are different");
        }
        else{
            System.out.println("somethings wrong");
        }

        System.out.println("\n" + auto1);
        System.out.println("\n" + auto3);

    }
}