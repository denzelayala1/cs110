/*
   Denzel Ayala
   CS110

    This program describes the Taxi class
    an Taxi is defined by its owner which is a person object, its make, model, year, mileage, 
    the number of passangers it can hold, if it is an SUV, 
    its Driver which is a person object, and the Driver ID number
*/
public class Taxi extends Automobile{
    
    private Person driver; // a person Object that contains the information for the taxi's driver
    private String ID; // The taxi's ID number

    /**
     *  Constructor for a taxi object 
     * @param name A string that represents the name of the owner of the vehicle
     * @param address A string that represents the address of the owner of the vehicle
     * @param phone A string that represents the phone number of the owner of the vehicle
     * @param mk A string that represents the make of the vehicle
     * @param mod A string that represents the model of the vehicle
     * @param yr An integer that represents the year of the vehicle's release
     * @param mile An integer that represents the miles on the vehicle
     * @param numPass An integer that represent the number of passangers the automobile can hold
     * @param suv A boolean that will indicate if this automobile is an SUV
     * @param drName A string that represents the name of the taxi driver
     * @param drAdrs A string that represents the taxi driver's address
     * @param drPhone A string that represents the taxi driver's phone number
     * @param drID A string that represents the ID for taxi
     */
    public Taxi(String name,   String address,
                String phone,  String mk,
                String mod,    int yr,
                int mile,      int numPass,
                boolean suv,   String drName,
                String drAdrs, String drPhone,
                String drID){

        // call the Automobile constructor
        super(name, address, phone, 
              mk, mod, yr, mile,
              numPass, suv);

        driver = new Person(drName, drAdrs, drPhone); // create new person object and set that as driver
        ID = drID; // set the taxi ID
    }

    //Getters
    /**
     * Gets the Taxi's driver
     * @return a Person object that is the driver of the taxi
     */
    public Person getDriver(){

        Person rtDriver = new Person( this.driver.getName(),
                                      this.driver.getAddress(),
                                      this.driver.getPhone());
        return rtDriver;                        
    }

    /**
     * Gets the Taxi ID
     * @return a string the is the Taxi ID
     */
    public String getID(){
        return ID;
    }


    //Setters
    /**
     * Sets the Driver of the taxi
     * @param name the driver's name
     * @param address the driver's address
     * @param phone the driver's phone number
     */
    public void setDriver(String name, String address, String phone){
        driver = new Person(name, address, phone);
    }

    /**
     * Sets the driver of the taxi
     * @param obj2 A person object that will be the driver
     */
    public void setDriver(Person obj2){
        driver = new Person(obj2.getName(),
                            obj2.getAddress(),
                            obj2.getPhone());
    }

    /**
     * Set's the taxi ID 
     * @param id a string that represent the taxi's ID
     */
    public void setID(String id){
        ID = id;
    }


    //Additional methods
    /**
     * 
     * @return A string that contains the critical information of a taxi
     */
    public String toString(){
        return super.toString() + ",\nDriver: " + driver.toString() + " ID#" + ID;
    }

    /**
     * 
     * @return Returns True if the two objects have the same automobile properties and the same driver and taxi ID
     */
    public boolean equals(Object obj2){
        Taxi tax2 = (Taxi)obj2;
        if(super.equals(tax2) &&
            this.driver.equals(tax2.getDriver()) &&
            this.ID.equals(tax2.getID()) ){

            return true;
            }
        else{
            return false;
        }
    }
}