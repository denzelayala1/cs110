/*
    Denzel Ayala
    CS110

    This program provides static methods to work with arrays

*/


import java.util.Scanner;
import java.io.*;

public class ArrayFunctions{

    /**
     * This method looks for a file with a provided name and then populates a provided array 
     * with the strings on each line of the file
     * @param fileName A string that is the name of the file we are looking for. 
     * @param nameArray An array of strings that will be populated from the file
     * @return Returns an integer that is how many lines (names) were added to the array.
     */
    public static int readArray(String fileName, String[] nameArray){

        int numberOfNames = 0; // counter for how many names will be added to the array


        try{

            // search for the file with the provided filename and start reading from it
            File file = new File(fileName); 
            Scanner scanFile = new Scanner(file);

            //read through the file line by line and populate the array with each line
            while(numberOfNames < nameArray.length && scanFile.hasNextLine()){
                nameArray[numberOfNames] = scanFile.nextLine();
                numberOfNames += 1;
            }

            scanFile.close();
        }
        catch(FileNotFoundException e){
            System.out.println("WARNING!!! The file was not found");
            return 0; 
        }
        catch(NullPointerException e){
            System.out.println("WARNING!!! The array appears to be empty");
            return 0;
        }

        return numberOfNames;
    }

    /**
     * Creates a file with the given name and populates the file with the contents of the provided string array
     * @param fileName A string that is the name of the file that will be created
     * @param nameArray An array of strings that will be used to populate the newly created file.
     * @return Returns true if the file was sucessfully written.
     */
    public static boolean writeArray(String fileName, String[] nameArray){

        int count  = 0; //index for array and lines on new file
        try{

            //create a new file and open an object to write to it
            FileWriter outFile = new FileWriter(fileName);
            PrintWriter printToFile = new PrintWriter(outFile);

            //write the contents of the array to the new file
            while(count < nameArray.length){
                printToFile.printf( (count + 1) + ": " + nameArray[count] + "\n");
                count += 1;
            }

            printToFile.close();
        }
        catch(NullPointerException e){
            System.out.println("The array is empty");
            return false;
        }
        catch(IOException e){
            System.out.println("There is an error with the file");
            return false;
        }
        
        return true;
    }
    
    /**
     * Takes in an array of strings and sorts the array in alphabetical order (a -> z)
     * @param nameArray An array of strings that will be sorted
     * @param numberOfNames An integer that is the last filled position in the array
     */
    public static void selectionSort(String[] nameArray, int numberOfNames){

        int startIndex, //the starting point of the for loop
            currentIndex, // the index of the string being evaluated
            minIndex; //the index of the lexigraphically lowest string
        String alphabeticalName; // the String of the minIndex
        try{
            //outter loop going throught he array checking the current start index
            for(startIndex = 0; startIndex < numberOfNames - 1; startIndex++){

                minIndex = startIndex;
                alphabeticalName = nameArray[startIndex];

                //inner loop checking for the lowest lexigraphical string
                for(currentIndex = startIndex + 1; currentIndex < numberOfNames; currentIndex++){
                    if(alphabeticalName.compareTo(nameArray[currentIndex]) > 0){
                        alphabeticalName = nameArray[currentIndex];
                        minIndex = currentIndex;
                    }
                }

                //swapping positions of the strings
                nameArray[minIndex] = nameArray[startIndex];
                nameArray[startIndex] = alphabeticalName;
            }
        }
        catch(ArrayIndexOutOfBoundsException e){
            System.out.println("WARNING!! This part of the list appears to not exist.");
        }
    }

    /**
     * Recursivly searches a sorted array of strings for a given string
     * @param sortedNameArray An array of strings that will be searched
     * @param first the starting point of the search in this itteration
     * @param last the last index that will be searched in this itteration 
     * @param desiredName the String that will be searched for
     * @return
     */
    public static int binarySearch(String[] sortedNameArray, int first, int last, String desiredName){

        //base case of the search
        if(first > last ){
            return -1;
        }

        //the middle point that will be compared to the desired string
        int middle;
        middle = (first + last) / 2;

        try{
            //Checks if the string at a given index is the same as the desired string
            if(sortedNameArray[middle].compareToIgnoreCase(desiredName) == 0){
                return middle;
            }
            //if the current indexed string is lower than the desired string it will perform another
            // search with the index after the current as the new starting point
            else if(sortedNameArray[middle].compareToIgnoreCase(desiredName) < 0){
                return binarySearch(sortedNameArray, middle + 1, last, desiredName);
                
            }
            //if the current indexed string is higher than the desired string it will perform another
            // search with the index before the current as the new end point
            else{
                return binarySearch(sortedNameArray, first, middle - 1 , desiredName);
            }
        }
        catch(NullPointerException e){
            System.out.println("WARNING!! The array appears to be empty.");
            return -1;
        }
        catch(ArrayIndexOutOfBoundsException e){
            System.out.println("WARNING!! This part of the list appears to not exist.");
            return -1;
        }
    }
}