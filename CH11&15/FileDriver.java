/*
    Denzel Ayala
    CS 110

    This program uses the ArrayFunctions class to search a file for a deisred baby name
*/
import java.util.Scanner;

public class FileDriver{

    public static void main(String[] args) {
        
        String  fileName, //the name of the file that will be searched
                nextInput = "This"; //place holder for input string

        int numOfNames = -1; //the number of names that should be looked at in the file
        Scanner keyboard = new Scanner(System.in); // input scanner

        //getting the name of the file that will be looked for
        System.out.println("What is the name of the file you want to look through?");
        fileName = keyboard.nextLine();
    
        // getting the number of names the user wants to look through
        System.out.println("How many names would you like to look through?");
        //while loop that will continue till user inputs a proper positive integer
        while(numOfNames < 0){
            try{
                nextInput = keyboard.nextLine(); //user input taken as a string
                numOfNames = Integer.parseInt(nextInput); //string searched for an integer
            }
            catch(NumberFormatException e){
                //if the inputed string doesnt contain an postive integer it will not crash the program but rather 
                //display this msg and propt the user again.
                if(numOfNames == -1){
                    System.out.printf("%s is not an integer.\nEnter a positive integer value\n", nextInput);
                }
                else{
                    System.out.println("Enter a positive integer value");
                }
            }
        }

        //create an array of strings of the inputted length to be used
        String[] nameArray = new String[numOfNames];

        
        int namesInArray; //the number of names in the array since it may not fill the array
        //reads the list of names out of the inputed file and adds them to the array while
        //keeping track of the number of names added.
        namesInArray = ArrayFunctions.readArray(fileName, nameArray);
        if(namesInArray < numOfNames){
            System.out.printf("The file only contained %d names.\n", namesInArray); 
            //if the array was only partially filled it lets the user know how may names were added
        }

        //sorts the array in alphabetical order
        ArrayFunctions.selectionSort(nameArray, namesInArray);
        
        String outFileName = "sorted_" + fileName;// name of the output file
        
        //writes the new file and lets the user know if it worked or not
        if( !ArrayFunctions.writeArray(outFileName, nameArray) ){
            System.out.println("Write was unsucessful.");
        }

        //Prompts user for their desired name
        System.out.println("What name would you like to look for?");
        String desiredName; //the desired name
        desiredName = keyboard.nextLine();

        //searches the sorted array for the desired name
        int nameLocation = ArrayFunctions.binarySearch(nameArray, 0, namesInArray, desiredName);

        if( nameLocation > 0){
            //tells the user where in the list the name was
            System.out.printf("%s was %d on the list", nameArray[nameLocation], nameLocation + 1);
        }
        else{
            //tells the user if the name was not found 
            System.out.printf("%s was not in the top %d names", desiredName, namesInArray);
        }
        keyboard.close();
    }
}