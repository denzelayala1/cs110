/* 
   Denzel Ayala
   CS110

   This is a program that will break down the number
   of eggs Jackie sold in a month in terms of
   gross, dozen and individual eggs
*/

import java.util.Scanner;

public class Eggs{
    public static void main(String [] args){

        // Initialization of variables and objects needed 
        Scanner usrIn = new Scanner(System.in); // creat a scanner to get to total number of eggs

        final  int numberInGross = 144, // number of eggs in a gross
                   numberInDozen = 12; // number of eggs in a dozen

         int grossEggs, // number of Gross eggs sold
             dozenEggs, // number of dozen eggs sold
             singleEggs, // number of individual eggs sold
             totalEggs; // total number of eggs sold
            

        // Prompting the user for the total eggs sales this month
        System.out.println("Jackie's Egg Layers");
        System.out.println("Please enter how many eggs you sell this month?");
        
        // insureing that only integer values are accepted
        totalEggs = usrIn.nextInt();

        // whole number calculation of th 3 major unit types
        grossEggs = totalEggs / numberInGross ; // calculating gross eggs
        dozenEggs = (totalEggs - (numberInGross* grossEggs)) / numberInDozen ; // calculatin dozen eggs
        singleEggs = totalEggs - (numberInGross * grossEggs) - (numberInDozen * dozenEggs) ; // calculating individual eggs

        // Reporting the breakdown of egg sales
        System.out.println("You sold " + grossEggs + " gross of eggs");
        System.out.println("You sold " + dozenEggs + " dozen of eggs");
        System.out.println("You sold " + singleEggs + " individual eggs");
        
        usrIn.close();
    }
}