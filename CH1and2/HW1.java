/*Jackie recently acquired a flock of rather productive egg laying chickens. As a side job, she has started an egg business. At the end
of the month, she’d like a breakdown of eggs sold. You are to write a program that determines how many grosses of eggs (a gross is
144 eggs), how many dozen (beyond the gross count), and how many remaining single eggs she sold. The user of the program will
enter a total count and the program will produce the desired output. Your program run should match the sample runs exactly, given
the same input. Your program should be named Eggs.java. You do not need to do input validation or exception handling.
*/
import java.util.Scanner;

import sun.tools.jconsole.ProxyClient.Snapshot;


public class HW1{
    public static void main(String [] args){

        Scanner usrIn = new Scanner(System.in);
        int grossEggs , dozenEggs, singleEggs, totalEggs; 

        System.out.println("Jackie's Egg Layers");
        System.out.println("Please enter how many eggs you sell this month?");
        totalEggs = usrIn.nextInt();

        grossEggs = totalEggs / 144 ; 
        dozenEggs = (totalEggs - (144* grossEggs)) / 12 ;
        singleEggs = totalEggs - (144 * grossEggs) - (12 * dozenEggs) ; 

        System.out.println("You sold " + grossEggs + " gross of eggs");
        System.out.println("You sold " + dozenEggs + " dozen of eggs");
        System.out.println("You sold " + singleEggs + " individual eggs");
        
    }
}