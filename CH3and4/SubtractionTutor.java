/* 
   Denzel Ayala
   CS110

   This program will generate 5 subtraction practice problems
   within a range specified by the user.
*/

import java.util.Scanner;
import java.util.Random;

public class  SubtractionTutor{
    public static void main(final String[] args) {

        //variable initialization
        int low, high, swap; //stores user input of low and high end of program range
        int rand1, rand2; //stores values that will be subtracted
        int answer, response; //stores the correct answer and the user response
        int loopCount, correctResponses = 0; //stores loop iterations and correct responses
        String welcome, intro; // Introduction Text
        String sLow, sHigh, badInput; // prompts for low and high value input

        final Scanner numInput = new Scanner(System.in); // to hold user input
        final Random numGen = new Random(); // to generate number between low and high value

        //introduction text
        welcome = "Welcome to the Subtraction Tutor";
        intro = "You will supply a low value and high value, defining the range \n" +
                "of values to be included in your quiz.  You will get 5 problems.\n" +
                "The values must be positive and no larger than 9999";

        //input messages
        sLow = "Low value :";
        sHigh = "High value :";
        badInput = "This value isn't between 1-9999. Please try again.";

        //introduction text
        System.out.printf("%-100s \n%-100s\n", welcome, intro);
        System.out.printf("%-11s  ", sLow);
        
        //get low end of range
        low = numInput.nextInt();

        //parse user input to be within 1-9999
        if (low < 1 || low > 9999){

            System.out.printf("%-100s\n", badInput);
            System.out.printf("%-11s  ", sLow);
            low = numInput.nextInt();
        }
        
        // get high end of range
        System.out.printf("%-12s  ", sHigh);
        high = numInput.nextInt();

        //parse user input to be within 1-9999
        if(high < 1 || high > 9999){

            System.out.printf("%-100s\n", badInput);
            System.out.printf("%-12s  ", sHigh);
            high = numInput.nextInt();
            }

        //make sure the user input for low is the lower value
        if( low > high){

            swap = low;
            low = high;
            high = swap;
        }

        //generate 5 subtraction problems
        for(loopCount = 0; loopCount < 5; loopCount++ ){

            //generate first random number within given range
            rand1 = numGen.nextInt(high) + 1;
            if( rand1 < low){
                rand1 = numGen.nextInt(high) + 1;
            }

            //generate second random number within given range
            rand2 = numGen.nextInt(high) + 1;
            if( rand2 < low){
                rand2 = numGen.nextInt(high) + 1;
            }

            //display subtraction problem with the larger of the two numbers on top
            if(rand1 > rand2){
                System.out.printf("%5d\n-%4d\n-----", rand1, rand2);
                answer = rand1 - rand2;
            }
            else{
                System.out.printf("%5d\n-%4d\n-----", rand2, rand1);
                answer = rand2 - rand1;
            }

            //prompt user for answer and record input
            System.out.printf("\nAnswer?  ");
            response = numInput.nextInt();

            //check if user calculated correctly
            if(response == answer){
                correctResponses++;
                System.out.println("Correct!\n\n");
            }
            else{
                System.out.printf("Incorrect! The answer is %d\n\n", answer);
            }
        }
        numInput.close();
        
        //declare end of quiz
        System.out.println("The quiz is over");

        //score user out of 5
        if(correctResponses == loopCount){
            System.out.println("Excellent! You got all 5 problems correct.");
        }
        else if(correctResponses > loopCount/2 && correctResponses < loopCount){
            System.out.println("You got over half correct, practice makes perfect!");
        }
        else{
            System.out.println("You got less than half correct, you need to spend more time practicing.");
        }
        
        
    }
}