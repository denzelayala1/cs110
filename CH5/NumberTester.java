/*
    Denzel Ayala
    CS110

    This program takes in a file of numbers and determines if each number is even, prime,
    or has repeated digits. It then puts this information into a new text file in a formatted table
*/
import java.io.*;
import java.util.Scanner;

/**
 * Main reads the input file and then generates and 
 * writes to a new file. It also creates and formats 
 * a table with the desired properties 
 */
public class NumberTester{
    public static void main(String[] args) throws IOException{

        // Create Scanner for user input
        Scanner keyboard = new Scanner(System.in);

        // Get location of numbers.txt from user
        System.out.println("\nPlease indicate complete file location\n" +
                            "Example: C:\\Users\\yourUserName\\Documents\\");
        String inputFile = keyboard.next();

        //Make sure the file is visable
        File number = new File(inputFile + "\\numbers.txt");
        if(number.exists() == false){
            System.out.println("The file does not appear to be there. Please check input.");
            System.exit(0);
        }

        //open numbers.txt to be read later
        Scanner inNumber = new Scanner(number);
        
        //create new file and set up ability to write to it
        PrintWriter outFile = new PrintWriter(inputFile + "\\numberSummary.txt");

        // print Headers to Summary
        String header1 = "Repeat";
        String header2 = "Number Digits Even Prime";
        outFile.printf("%13s\n%s\n",header1,header2);
    
        //Loop that checks each line of the file
        while(inNumber.hasNextLine()){
            
            // storage variables for the table values
            char a, b, c;
            int num = inNumber.nextInt(); //checks each integer in the file 

            // checks if number has repeated digits and assigns character for table
            if(isRepeatedDigits(num) == true){
                a = '+';
            }
            else{ 
                a = '-';
            }

            // checks if number is Even and assigns character for table
            if(isEven(num) == true){
                b = '+';
            }
            else{ 
                b = '-';
            }

             // checks if number is prime and assigns character for table
            if(isPrime(num) == true){
                c = '+';
            }
            else{ 
                c = '-';
            }

            //Writes the current line to the txt file with proper formating and spacing
            outFile.printf("%4d%7c%5c%6c\n",num,a,b,c);
        }

        //closeing files
        outFile.close();
        inNumber.close();
        keyboard.close();
    }


    /**
     * The isPrime method checks if a number is prime and returns a boolean
     * if prime 
     * @param num an integer being checked for primality
     * @return if num is prime returns true, if num is NOT prime returns false
     */
    public static boolean isPrime(int num){

        //checks if number is even    
        if(isEven(num) == true){
            return false;
        }

        // loop that checks all posible odd numbers up to half 
        // the input number
        for(int checker = 3; checker < num/2; checker++){

            if(isDivisiable(num, checker) == true){
                return false;
            }
            
        }
        
        return true;
    }


    /**
     * This method checks if a number is divisable without remainder by another number
     * @param numerator the number that will be divided
     * @param divisor the number in the denomenator that will be used to divide
     * @return  true if numerator is divisable, false otherwise
     */
    public static boolean isDivisiable(int numerator, int divisor){

        if(numerator % divisor == 0){
            return true;
        }
        else{
            return false;
        }
    }


    /**
     * This method checks if a number is even valued by seeing if its divisable by 2
     * @param num the number that will be divided
     * @return If the number is even returns true, else returns false
     */
    public static boolean isEven(int num){
        if(num % 2 == 0 ){
            return true;
        }
        else{
            return false;
        }
    }


    /**
     * This method converts a number into a string then checks to see if there are 
     * any repeated characters
     * @param num number that will be tested for repeated digits
     * @return returns true if there are repeated digits else  returns false
     */
    public static boolean isRepeatedDigits(int num){

        // converts int to string
        String strNum = Integer.toString(num);

        // loop that checks two adjacent characters to see if they are the same
        int i = 1;
        while(i < strNum.length()){

            if(strNum.charAt(i-1) == strNum.charAt(i)){
                return true;
            }
            i++;
        }
        return false;
    }
}