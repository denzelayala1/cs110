import java.io.*;
import java.util.Scanner;


public class TestIsRep{

    public static void main(String[] args) {

        int num  = 1234;
        if(isRepeatedDigits(num) == false)
        {
            System.out.println("there are no repeats " + num);
        }
        else{
            System.out.println("IT REPEATS " + num);
        }
    }


    public static boolean isRepeatedDigits(int num){

        String strNum = Integer.toString(num);

        int i = 1;
        while(i < strNum.length()){

            System.out.println(strNum.charAt(i));

            if(strNum.charAt(i-1) == strNum.charAt(i)){
                return true;
            }
    
            i++;
        }

        return false;
    }
}