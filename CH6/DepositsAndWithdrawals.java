/*
   Denzel Ayala
   CS110

   This program creates an instance of the SavingsAccount class.
   It then deposits and withdraws money for the month and displays
   the accrued interest and final balance.

*/

import java.util.Scanner;
import java.io.*;

public class DepositsAndWithdrawals{

    /**
     * The main function takes in the users interest rate and the loction of 
     * the deposit and withdrawal files then displays deposits, withdrawals,
     * earned interest and final balance in a formatted manor
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException{

        // create scanner for user input
        Scanner keyboard = new Scanner(System.in);


        // Ask for account interest rate and store in "interest" variable
        System.out.print("\nEnter the savings account's annual interest rate: ");
        double interest;
        interest = keyboard.nextDouble();


        /// Ask for account initial balance and store in "balance" variable
        System.out.print("Starting Balance: $500");
        double balance = 500;

        //creating an instance of the SavingsAccount class
        SavingsAccount userAccount = new SavingsAccount(balance, interest);


        // Get location of deposits.txt from user
        System.out.println("\nPlease indicate complete file path for depoits\n" +
                            "Example: C:\\Users\\yourUserName\\Documents\\...");
        String inputFile1 = keyboard.next();

        //Make sure the file is visable
        File fileDeposit = new File(inputFile1 + "\\Deposits.txt");
        if(fileDeposit.exists() == false){
            System.out.println("The file does not appear to be there. Please check input.");
            System.exit(0);
        }

        //opening the file contaning the months deposits
        Scanner scanDeposits = new Scanner(fileDeposit);


        // Get location of Withdrawals.txt from user
        System.out.println("\nPlease indicate complete file path for Withdrawals\n" +
                            "Example: C:\\Users\\yourUserName\\Documents\\...");
        String inputFile2 = keyboard.next();

        //Make sure the file is visable
        File fileWithdrawals = new File(inputFile2 + "\\Withdrawals.txt");
        if(fileWithdrawals.exists() == false){
            System.out.println("The file does not appear to be there. Please check input.");
            System.exit(0);
        }

        //opening the file contating the months withdrawals
        Scanner scanWithdrawals = new Scanner(fileWithdrawals);        


        //Depositing the money into the user's account
        while(scanDeposits.hasNextLine()){

            //reading the ammount being deposited and depositing into account
            double deposited = scanDeposits.nextDouble();
            userAccount.deposit(deposited);

            //displaying the ammount deposited and the balance after deposit
            System.out.printf("Deposit: $%.2f, balance = $%.2f\n", deposited, userAccount.getBalance());
        }

        while(scanWithdrawals.hasNextLine()){

            //reading the ammount being withdrawn and withdrawing from account
            double withdrawn = scanWithdrawals.nextDouble();
            userAccount.withdrawal(withdrawn);

            // displaying the ammount withdrawn and the balance after withdrawal
            System.out.printf("Withdraw: $%.2f, balance = $%.2f\n", withdrawn, userAccount.getBalance());
        }

        //calculating and adding the interest at the end of the month
        userAccount.addInterest();
        
        // displaying final monthly balance and accrued monthly interest
        System.out.printf("Interest earned: $%.2f\nEnding balance: $%.2f",
                           userAccount.getLastInterest(), userAccount.getBalance());


        // closing all scanners
        keyboard.close();
        scanDeposits.close();
        scanWithdrawals.close();
    }
}