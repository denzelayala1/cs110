/*
Denzel Ayala
   CS110

   This program is used to create instances of Savings Accounts with a set interest rate. 
   Money can be deposited and withdrwan into the savings account.
*/

public class SavingsAccount{

    // fields
    private double balance; //stores account balance
    private double interestRate; //Account anual interest rate
    private double lastInterest = 0; //Value of previous months interest earned


    //CONSTRUCTORS

    /**
     * Constructs an instance of the Savings account class
     * @param initBal the inital balance that this account contains
     * @param intRate the annual interest rate on the account
     */
    public SavingsAccount(double initBal, double intRate) {

        balance = initBal;
        interestRate = intRate/100;
    }


     //CLASS METHODS

     /**
      * Subtracts an ammount of money from the savings account
      * @param ammount the ammount to be removed
      */
     public void withdrawal(double ammount) {
        balance -= ammount;
    }

    /**
     * Adds an ammount of money to the savings account
     * @param ammount the ammount to added
     */
    public void deposit(double ammount) {

        balance += ammount;
    }

    /**
     * Calculates the monthly interest and adds it to the total balance
     */
    public void addInterest() {

        lastInterest = balance * (interestRate / 12); 
        balance += lastInterest;
    }


    //GETTER METHODS

    /**
     * 
     * @return The current balance on the account
     */
    public double getBalance() {
        return balance;
    }

    /**
     * 
     * @return Returns the account's annual interest rate
     */
    public double getInterestRate(){
        return interestRate;
    }

    /**
     * 
     * @return Returns the last months interest earned
     */
    public double getLastInterest(){
        return lastInterest;
    }


   
}