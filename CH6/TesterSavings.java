/*Test the class in a program that calculates the balance of a savings account 
at the end of a period of time. It should 
+ask the user for the annual interest rate, 
+the starting balance, 
+and the number of months that have passed since the account was established. 

A loop should then iterate once for every month, 
performing the following:

a. Ask the user for the amount deposited into the account during the month. Use the 
class method to add this amount to the account balance.

b. Ask the user for the amount withdrawn from the account during the month. Use the 
class method to subtract this amount from the account balance.

c. Use the class method to calculate the monthly interest.

After the last iteration, the program should display the ending balance, the total 
amount of deposits, the total amount of withdrawals, and the total interest earned.
*/

import java.util.Scanner;


public class TesterSavings{

    public static void main(String[] args){

        Scanner keyboard = new Scanner(System.in);
        double interestRate;
        double balance;
        int accountAge;


        System.out.println("\nWhat is your anual interest rate (ex: enter 2.5% as 2.5) ?");
        interestRate = keyboard.nextDouble();

        System.out.println("What is your initial balance?");
        balance = keyboard.nextDouble();

        System.out.println("How many months have passed since the account was established?");
        accountAge = keyboard.nextInt();

        SavingsAccount userAccount = new SavingsAccount(balance, interestRate);


        double totalDeposits = 0.0;
        double totalWithdrawals = 0.0;
        double totalInterest = 0.0;

        for(int i = 1; i <= accountAge; i++){

            System.out.printf("How much was deposited in month %d?\n", i);
            double deposit = keyboard.nextDouble();
            userAccount.deposit(deposit);
            totalDeposits += deposit;

            System.out.printf("How much was withdrawn in month %d?\n", i);
            double withdrawal = keyboard.nextDouble();
            userAccount.withdrawal(withdrawal);
            totalWithdrawals += withdrawal;

            userAccount.addInterest();
            totalInterest += userAccount.getLastInterest();
        }

        System.out.printf("Activity after the past %d months\n" +
                         "Final balance is: $%.2f\n" +
                         "Total deposit ammount: $%.2f\n" +
                         "Total withrawal ammount: $%.2f\n"+
                         "Total interest earned: $%.2f\n",
                         accountAge, userAccount.getBalance(), 
                         totalDeposits, totalWithdrawals, totalInterest);
    }
}