/*
   Denzel Ayala
   CS110

   This program defines the parameters for the Author class. 
   An author is defined by their first name, last name and their email addresss
*/

public class Author{

   //Fields
   private String firstName; // stores authors first name
   private String lastName; // stores authors last name
   private String emailAddress; // stores authors email address
   
   
   /**
   * Constructs an instance of the Author class
   * @param first The author's first name
   * @param last The author's last name
   * @param email The author's email address
   */
   public Author(String first, String last, String email){
   
      firstName = first;
      lastName = last;
      emailAddress = email;
   }
   
   
   //Author Setter methods
   
   /**
   *This method sets the author's first name
   *@param first The Author's first name
   */ 
   public void setFirstName(String first){
      firstName = first;
   }
   
   /**
   *This method sets the author's last name
   *@param last The Author's last name
   */
   public void setLastName(String last){
      lastName = last;
   }
   
   /**
   *This method sets the author's email address
   *@param email The Author's email address
   */
   public void setEmailAddress(String email){
      emailAddress = email;
   }
   
   //Author Getter methods
   /**
   *This method returns the author's first name
   *@return  The Author's first name
   */
   public String getFirstName(){
      return firstName;
   }
   
    /**
   *This method returns the author's last name
   *@return  The Author's last name
   */
   public String getLastName(){
      return lastName;
   }
   
    /**
   *This method returns the author's email address
   *@return  The Author's email address
   */
   public String getEmailAddress(){
      return emailAddress;
   }
   
   
   /**
   * toString method
   * @return a string indicating the author's first and last name and their email address
   */
   public String toString(){
      return firstName + " " + lastName + " <" + emailAddress + ">";
   }
   

    /**
   * checks if an author object is equal to this author object
   *@return  True if the two authors are the same, else returns false
   */  
   public boolean equals(Author obj2){
      
      boolean status;
      if(obj2.getFirstName() == this.firstName && obj2.getLastName() == this.lastName){
         status = true;
      }
      else{
         status = false;
      }
      return status;
   }
}
