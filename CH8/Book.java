/*
   Denzel Ayala
   CS110

   This program defines the parameters for a book class. 
   A book is defined by its author, publisher, ISBN-10 #, title and the number of pages it contains
*/

public class Book{
   
   //Fields
   String isbn; //stores the book's ISBN number as a string
   String title; //stores the book's title
   String publisher; //stores the book's publisher
   Author author; //stores the book's author object
   int numPages; //stores the number of pages the book has
   
   //Constructors
   
   /**
   * Constructs a instance of the book class
   *@param isbn a String that is the ISBN numer of the book
   *@param title a String that is the title of the book
   *@param author an Author object that is the author of the book
   *@param publisher a String that the publisher of the book
   *@param numPages an integer that represents the number of pages in the book
   */
   public Book(String isbn, String title, Author author, String publisher, int numPages){
      this.isbn = isbn;
      this.title = title;
      this.author = new Author(author.getFirstName(), author.getLastName(),
                               author.getEmailAddress());
      this.publisher = publisher;
      this.numPages = numPages;
   }
   
   /**
   * A constructor that takes a book object as an input
   */
   public Book(Book obj2){
   
      this.isbn = obj2.getISBN(); // extracts the ISBN number from the passed in Book object
      this.title = obj2.getTitle(); // extracts the title from the passed in Book object
      this.author = new Author(obj2.author.getFirstName(), obj2.author.getLastName(), 
                              obj2.author.getEmailAddress()); // extracts the Author object from passed in Book object
      this.publisher = obj2.getPublisher(); // extracts the publisher from the passed in Book object
      this.numPages = obj2.getNumPages(); // extracts the number of pages from the passed in Book object
   }
   
   
   //Setters
   /**
   * This method sets the book's ISBN number
   * @param isbn The Book's ISBN number
   */
   public void setISBN(String isbn){
      this.isbn = isbn;
   }
   
   /**
   * This method sets the book's title
   * @param title The Book's title
   */
   public void setTitle(String title){
      this.title = title;
   }
   
   /**
   * This method sets the book's author using an Author object
   * @param author The Book's author as an author object
   */
   public void setAuthor(Author author){
      this.author = new Author(author.getFirstName(), author.getLastName(),
                               author.getEmailAddress());
   }
   
   /**
   * This method sets the book's publisher
   * @param publisher The Book's publisher 
   */
   public void setPublisher(String publisher){
      this.publisher = publisher;
   }
   
   /**
   * This method sets the number of pages in the book
   * @param numPages The number of pages in the book
   */
   public void setNumPages(int numPages){
      this.numPages = numPages;
   }
   
   
   //Getters
   /**
   * 
   * @return The ISBN of the book as a string
   */
   public String getISBN(){
      return isbn;
   }
   
   /**
   * 
   * @return the title of the book as a string
   */
   public String getTitle(){
      return title;
   }
   
   /**
   * 
   * @return the author of the book as an Author object
   */
   public Author getAuthor(){
      return new Author(this.author.getFirstName(), this.author.getLastName(), this.author.getEmailAddress());
   }
   
   /**
   * 
   * @return the publisher of the book as a string
   */
   public String getPublisher(){
      return publisher;
   }
   
   /**
   * 
   * @return the number of pages in the book as an integer
   */
   public int getNumPages(){
      return numPages;
   }
   
  
  /**
  *
  * @return the title, author, ISBN number, and the number of pages  back in a formatted string
  */
   public String toString(){
      return title + ", " + author + " (ISBN-10 #" + isbn + ", " + numPages + " pages)";
   }
   
   /**
   *
   * @return true if the books have the same title, author and ISBN, else returns false
   */
   public boolean equals(Book obj2){
      
      boolean status;
      if(obj2.getTitle() == title && obj2.author.equals(this.author) && obj2.getISBN() == isbn){
         status = true;
      }
      else{
         status = false;
      }
      return status;
   }
}
