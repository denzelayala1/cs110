import java.util.ArrayList;

public class Tester{
    public static void main(String[] args){

        String user1 = "frank",
                user2 = "John",
                post1 = "this is a test",
                post2 = "we will see if it works";
        Tweet test1 = new Tweet(user1,post1);
        Tweet test2 = new Tweet(user2,post2);
        Tweet copy1 = new Tweet(test1);

        if(test1.equals(copy1) && !test1.equals(test2)){
            System.out.println("\nthe equals method and copy constructor work\n");
        }
        else{
            System.out.println("it failed");
        }

        if(test1 == copy1){
            System.out.println("these are the same address\n");
        }
        else{System.out.println("these are different objects\n");}


        System.out.println(test2);

        ArrayList<Integer> letSee = new ArrayList<>(100);
        letSee.add(1);
        letSee.add(2);
        letSee.add(100);
        System.out.println("the size of this array is " + letSee.size());
    }
}