/*
   Denzel Ayala
   CS110

    This program defines the Tweet class
    a Tweet is defined by the screen name and the content of the tweet
*/


public class Tweet{

    private String  screenName, // Screen name of the tweet
                    content; // the 140 character message in the tweet

    public final  int MAX_CHARS = 140; // the max characters allowed in a tweet


    //Constructors
    /**
     *  Constructs and Instance of the the Tweet class 
     * @param name a String that defines the screen name of the tweet 
     * @param cont a String that is the 140 character messafe in the tweet
     */
    public Tweet(String name, String cont){
        screenName = name;
        content = maxChar(cont);
    }

    /**
     *  This constructor creates a deep copy of the tweet that gets passed in
     * @param tweet a tweet object
     */
    public Tweet(Tweet tweet){
        screenName = tweet.screenName;
        content = tweet.content;
    }


    //Setters
    /**
     *  Sets the screen name of a tweet
     * @param name a String that defines the screen name of the tweet 
     */
    public void setScreenName(String name){
        screenName = name;
    }

    /**
     *  Sets the 140 character message of a tweet
     * @param contenta String that is the 140 character messafe in the tweet
     */
    public void setContent(String content){
        this.content = maxChar(content);
    }


    //Getters
    /**
     * 
     * @return The screen name of the tweet
     */
    public String getScreenName(){
        return screenName;
    }

    /**
     * 
     * @return the 140 character message of the tweet
     */
    public String getContent(){
        return content;
    }


    //Additional Methods

    /**
     * @return Returns the tweet formated as: @ScreenName
     *                                        "this is the tweet"
     */
    public String toString(){
        return "@" + screenName + "\n\"" + content + "\"";
    }

    /**
     *  Checks if two tweets are equal
     * @param str2 the tweet that is being checked
     * @return True if the tweets have the same screen name and content
     */
    public Boolean equals(Tweet str2){
        if(this.screenName.equals(str2.screenName) && 
            this.content.equals(str2.content)){
            return true;
        }
        else{
            return false;
        }    
    }

    /**
     *  This method truncates a String if its over the max character count
     * @param post a string input that will become a tweet
     * @return a string that has a maximum of 140 characters
     */
    private  String maxChar(String post){

        String outContent = "";
        int counter = 0;

        while(counter < MAX_CHARS && counter < post.length()){
            outContent += post.charAt(counter);
            counter++;
        }
        return outContent;
    }
}