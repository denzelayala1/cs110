/*
   Denzel Ayala
   CS110

   This program tests the capabilites of the Tweet and TwitterFeed classes by creating twitter feed instances
   and displaying them.
*/
import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

public class TwitterDriver{
    public static void main(String[] args) throws IOException{
        
        Scanner keyboard = new Scanner(System.in); // user input

        // Get location of deposits.txt from user
        System.out.println("\nPlease indicate complete file path for depoits\n" +
                            "Example: C:\\Users\\yourUserName\\Documents\\...");
        String inputFile = keyboard.next();

        // check if file exists 
        File feedFile = new File(inputFile + "\\feed1.txt");
        if(feedFile.exists() == false){
            System.out.println("The file does not appear to be there. Please check input.");
            System.exit(0);
        }

        //Create an instance of a twitterfeed using feed1.txt
        Scanner scanFeed = new Scanner(feedFile);
        String userName = scanFeed.nextLine();
        TwitterFeed jeffsFeed = new TwitterFeed(userName);

        //populate the twitter feed created above
        while(scanFeed.hasNextLine()){

            String feedName = scanFeed.nextLine();
            String feedTweet = scanFeed.nextLine();

            jeffsFeed.addTweet(feedName, feedTweet);
        }

        //Print out the twitter feed
        System.out.println(jeffsFeed);

        // check the functionality of the tweetsBy method by creating a new array of tweets
        ArrayList<Tweet> billTweets = jeffsFeed.tweetsBy("BillGates");

        //turn this array of tweets into a feed so it can be printed in a nice format
        TwitterFeed billsFeed = new TwitterFeed("BillGates");
        
        //populate the new feed
        int j = 0;
        while(j < billTweets.size()){
            billsFeed.addTweet(billTweets.get(j));
            j++;
        }

        //print out twitter feed made using the tweetsBy method
        System.out.println(billsFeed);


        scanFeed.close();
        keyboard.close();
    }
}