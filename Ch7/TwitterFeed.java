/*
   Denzel Ayala
   CS110

   This program defines the TwitterFeed class
   Its defined by the user screen name and an array list that contains tweets
*/
import java.util.ArrayList;

public class TwitterFeed{


    private String screenName; // the user's screen name
    private final int INITIAL_CAPACITY = 100; // the inital feed capacity
    private ArrayList<Tweet> feed; 

    //Constructors
    /**
     * Creates a twitter feed
     * @param name the user's screen name 
     */
    public TwitterFeed(String name){
        screenName = name;
        feed = new ArrayList<Tweet>(INITIAL_CAPACITY);
    }


    //Getters
    /**
     * 
     * @return Twitter Feed user's screen name
     */
    public String getScreenName(){
        return this.screenName;
    }

    /**
     * Creates a deep copy of the twitter feed
     * @return The user's twitter feed
     */
    public ArrayList<Tweet> getFeed(){

        ArrayList<Tweet> getFeed = new ArrayList<Tweet>(this.feed.size()); //create new array to return
        int i = 0;

        //populate the array with the content of the origional array
        while(i < this.feed.size()){
            
            Tweet newTweet = new Tweet(this.feed.get(i));
            getFeed.add(newTweet);
            i++;
        }

        return getFeed;
    }


    //Other Methods
    /**
     * This method adds a tweet to the user's twitter feed
     * @param name screen name of the person who tweeted
     * @param content the content of the tweet
     */
    public void addTweet(String name, String content){
        Tweet newTweet = new Tweet(name, content);
        this.feed.add(newTweet);
    }

    /**
     *  This method adds a tweet to the user's twitter feed
     * @param tweet the tweet that is being added
     */
    public void addTweet(Tweet tweet){
        Tweet newTweet = new Tweet(tweet);
        this.feed.add(newTweet);
    }

    /**
     * 
     * @return The user's formatted twitter feed
     */
    public String toString(){

        String header = "\n"+ this.screenName + "'s Feed\n-------------------------------------------------\n";
        String out = "";
        int i = 0;

        while(i < this.feed.size()){
            out += this.feed.get(i).toString() +"\n";
            i++;
        }
        return header + out;
    }

    /**
     * This method will find all the tweets from a specific person in the user's twitter feed
     * @param name the name of the person we want to find
     * @return An array list of the desired person's tweets
     */
    public ArrayList<Tweet> tweetsBy(String name){

        ArrayList<Tweet> returnFeed = new ArrayList<Tweet>(INITIAL_CAPACITY);
        int i = 0;

        while(i < this.feed.size()){

            if(name.equals( this.feed.get(i).getScreenName() )){
                Tweet newTweet = new Tweet(this.feed.get(i));
                returnFeed.add(newTweet);
            }

            i++;
        }

        return returnFeed;

    }
}