public class FinalTestSpace{
   
   public static void main(String[]  args){
   
      Team t1 = new Team("basketball", "fred");
      Team t2 = new Team("football", "bill");
      Team t3 = t1;
      Team t4 = new Team(t1);
      
      
      System.out.println(Boolean.toString( t1.equals(t4) ));
      System.out.println(Boolean.toString( t1 == t4 ));
      
      System.out.println(Integer.toString(sumNums(9,0) ));
      
      
   }


   public static int sumNums(int n, int inital){
      int sum = inital;
      if (n == 0){
         return sum;
      }
      else{
         sum += n;
         return sumNums(n-1, sum);
      }
   }
}