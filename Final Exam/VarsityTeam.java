public class VarsityTeam extends Team{

   private double funding;
   private int numScholarships;
   
   public VarsityTeam(String sport, String coach, double funds, int scholarships){
   
      super(sport, coach);
      funding = funds;
      numScholarships = scholarships;
      
   }
   
   
   public void setFunding(double funding){
      
      this.funding = funding;
      
   }
   
   
   public int getNumScholarships(){
   
      return numScholarships;
   
   }
   
}