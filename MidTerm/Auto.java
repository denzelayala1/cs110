public class Auto{

   private String make;
   private String model;
   private int year;
   private int mileage;
   
   public Auto(String iMake, String iModel, int iYear, int iMileage){
   
      make = iMake;
      model = iModel;
      year = iYear;
      mileage = iMileage;
   }
   
   public Auto(String iMake, String iModel, int iYear){
   
      make = iMake;
      model = iModel;
      year = iYear;
      mileage = 0;
   }
   
   
   public String getMake(){
      return make;
   }
   
   public String getModel(){
      return model;
   }
   
   public int getYear(){
      return year;
   }
   
   public int getMileage(){
      return mileage;
   }
   
   
   public void setMake(String iMake){
      make = iMake;
   }
   
   public void setModel(String iModel){
      model = iModel;
   }
   
   public void setYear(int iYear){
      year = iYear;
   }
   
   public void setMileage(int iMileage){
      mileage = iMileage;
   }
   
   public String toString(){
      
      return year + " " + make + " " + model +
             " with " + mileage + " miles";
   }
}