public class Driver{

   public static void main(String[] args){
      
      Card c1, c2, c3;
      
      c1 = new Card(1,0);
      c2 = new Card(4,1);
      c3 = new Card(4,1);
      
      Card c4 = c3;
      
      if(c1 == c2){
         System.out.println("These are the same object addresses");
      }
      else{
         System.out.println("These are NOT the same object addresses");
      }
      
      
      if(c3.getSuit() == c4.getSuit() && 
         c3.getRank() == c4.getRank() ){
         
         System.out.println("These are the same card");
         
      }
      else{
         System.out.println("These are NOT the same card");
      }
    
   }
   
}