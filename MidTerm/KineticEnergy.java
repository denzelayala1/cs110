import java.util.Scanner;

public class KineticEnergy{
   
   public static void main(String[] args){
      
      double mass;
      double velocity;
      double energy;
      Scanner keyboard;
      
      keyboard = new Scanner(System.in);
      
      System.out.print("What is object's mass? ");
      mass = keyboard.nextDouble();
      
      System.out.print("What is object's velocity? ");
      velocity = keyboard.nextDouble();
      
      energy = kineticEnergy(mass, velocity);
      System.out.printf("The object's kinetic energy is %.1f joules", energy);
      
   }
   
   public static double kineticEnergy(double m, double v){
      double ke = 0.5 * m * v * v;
      return ke;
   } 
}