/**Exception thrown when a List is accessed with an illegal index  */
public class ListIndexOutOfBoundsException extends IndexOutOfBoundsException
{
     /**
    *
    */
    private static final long serialVersionUID = 1L;

    public ListIndexOutOfBoundsException(String s) 
     {
          super(s);
     }  
}  