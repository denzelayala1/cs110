public class ListTester2
{

	public static void main(String [] args)
	{
      final int N = 5000;
	   long time1,time2, diff;
      ListInterface<Integer> list1 = new ListArrayBased<>();
      ListInterface<Integer> list2 = new ListReferenceBased<>();
	  
	  /*
	  //Adding at the BEGINING of the list

	  //Array Based
	  time1 = System.currentTimeMillis();
	  int rounds = N;
	  for(int i = 0; i < rounds; i++){
		  list1.add(1, i);
	  }
	  time2 = System.currentTimeMillis();
	  diff = time2-time1;
	  System.out.println("\nArray based method " + Long.toString(diff) + 
	  						" ms to ADD " + Integer.toString(rounds) + " items to the BEGINING");

	  //Reference based
	  time1 = System.currentTimeMillis();
	  rounds = N;
	  for(int i = 0; i < rounds ; i++){
		  list2.add(1, i);
	  }
	  time2 = System.currentTimeMillis();
	  diff = time2-time1;
	  System.out.println("Reference based method " + Long.toString(diff) + 
	  						" ms to ADD " + Integer.toString(rounds) + " items to the BEGINING\n");
   

	  //Adding to the END of the list
	  //Array 
	  time1 = System.currentTimeMillis();
	  rounds =  N;
	  for(int i = 0; i < rounds; i++){
		  int end = list1.size();
		  list1.add(end, i);
	  }
	  time2 = System.currentTimeMillis();
	  diff = time2-time1;
	  System.out.println("Array based method " + Long.toString(diff) + 
							  " ms to ADD " + Integer.toString(rounds) + " items to the END");
							  
	  //Reference

	  time1 = System.currentTimeMillis();
	  rounds = N ;
	  for(int i = 0; i < rounds; i++){
		  int end = list2.size();
		  list2.add(end, i);
	  }
	  time2 = System.currentTimeMillis();
	  diff = time2-time1;
	  System.out.println("Reference based method " + Long.toString(diff) + 
	  						" ms to ADD " + Integer.toString(rounds) + " items to the END\n");


	  //REMOVING at the BEGINING of the list

	  //Array Based
	  time1 = System.currentTimeMillis();
	  rounds = N ;
	  for(int i = 0; i < rounds ; i++){
		  list1.remove(1);
	  }
	  time2 = System.currentTimeMillis();
	  diff = time2-time1;
	  System.out.println("Array based method " + Long.toString(diff) + 
	  						" ms to REMOVE " + Integer.toString(rounds) + " items from the BEGINING");

	  //Reference based
	  time1 = System.currentTimeMillis();
	  rounds =  N ;
	  for(int i = 0; i < rounds; i++){
		  list2.remove(1);
	  }
	  time2 = System.currentTimeMillis();
	  diff = time2-time1;
	  System.out.println("Reference based method " + Long.toString(diff) + 
	  						" ms to REMOVE " + Integer.toString(rounds ) + " items from the BEGINING\n");
   

	  //REMOVING from END of the list
	  //Array 
	  time1 = System.currentTimeMillis();
	  rounds = N  ;
	  for(int i = 0; i < rounds; i++){
		  int end = list1.size();
		  list1.remove(end);
	  }
	  time2 = System.currentTimeMillis();
	  diff = time2-time1;
	  System.out.println("Array based method " + Long.toString(diff) + 
							  " ms to REMOVE " + Integer.toString(rounds) + " items from the END");
							  
	  //Reference
	  time1 = System.currentTimeMillis();
	  rounds = N  ;
	  for(int i = 0; i < rounds; i++){
		  int end = list2.size();
		  list2.remove(end);
	  }
	  time2 = System.currentTimeMillis();
	  diff = time2-time1;
	  System.out.println("Reference based method " + Long.toString(diff) + 
	  						" ms to REMOVE " + Integer.toString(rounds) + " items from the END\n");
*/

      //TRAVERSING LISTS
      
      //populating lists
      //populating array
      int rounds = 100;
      for(int i = 1; i <= rounds; i++){
        int end = list1.size() + 1;
        list1.add(end, i);
        }
      //populating reference
      for(int i = 1; i <= rounds ; i++){
        list2.add(1, i);
        }
      //ARRAY traverse
      time1 = System.currentTimeMillis();
      displayList(list1);
      time2 = System.currentTimeMillis();
      diff = time2-time1;
	  System.out.println("Array based method " + Long.toString(diff) + 
                              " ms to traverse " + Integer.toString(rounds) + " items");
    
      //reference traverse
      time1 = System.currentTimeMillis();
      displayList(list2);
      time2 = System.currentTimeMillis();
      diff = time2-time1;
	  System.out.println("reference based method " + Long.toString(diff) + 
	  						" ms to traverse " + Integer.toString(rounds) + " items\n");
	  

   	}




   // Traverse list displaying data in each item
   // I supressed the output for your testing.
	public static void displayList(ListInterface<Integer> list)
	{
		for (int i = 1; i<=list.size();i++){
			System.out.println(list.get(i));
         list.get(i); 
        }

	
	
	}
}