/*
   Denzel Ayala
   CS110

    This program describes the Automobile class
    an Automobile is defined by its owner which is a person object, its make, model, year, mileage, 
    the number of passangers it can hold, if it is an SUV, 
*/
public class Automobile extends Vehicle{

    private int numPassengers; //the number of passangers the automobile can hold
    private boolean isSUV; //if the automobile is an SUV

    //Constructors
    /**
     * 
     * @param name A string that represents the name of the owner of the vehicle
     * @param address A string that represents the address of the owner of the vehicle
     * @param phone A string that represents the phone number of the owner of the vehicle
     * @param mk A string that represents the make of the vehicle
     * @param mod A string that represents the model of the vehicle
     * @param yr An integer that represents the year of the vehicle's release
     * @param mile An integer that represents the miles on the vehicle
     * @param numPass An integer that represent the number of passangers the automobile can hold
     * @param suv A boolean that will indicate if this automobile is an SUV
     */
    public Automobile(String name, String address,
                      String phone, String mk,
                      String mod, int yr,
                      int mile, int numPass,
                      boolean suv){
 
        // calls vehicle constructor
        super(new Person(name, address, phone),
                mk, mod, yr, mile); 

        numPassengers = numPass; // sets the number of passangers the automobile can carry
        isSUV = suv; // sets if the automobile is an SUV
    }

    
    //Getters
    /**
     * Gets the number of passangers the automobile can carry
     * @return the number of passangers the automobile can carry
     */
    public int getNumPassangers(){
        return numPassengers;
    }

    /**
     * Gets if the automobile is an SUV or not
     * @return if the automobile is an SUV or not
     */
    public boolean getIsSUV(){
        return isSUV;
    }


    //Setters
    /**
     * Sets the number of passangers in the automobile
     * @param numPassengers the number of passangers in the automobile
     */
    public void setNumPassangers(int numPassengers){
        this.numPassengers = numPassengers;
    }

    /**
     * Sets if the automobile is an SUV
     * @param isSUV if the automobile is an SUV
     */
    public void setIsSUV(boolean isSUV){
        this.isSUV = isSUV;
    }


    //Additional methods
    /**
     * 
     * @return A string that contains the critical information of an automobile
     */
    public String toString(){
        if(isSUV){
            return super.toString() + " " + numPassengers + " passangers SUV";
        }
        else{
            return super.toString() + " " + numPassengers + " passangers";
        }
    }

    /**
     * 
     * @return true if the automobile have the same vehicle properties, number of passangers, and SUV condition
     */
    public boolean equals(Object obj2){
        Automobile auto2 = (Automobile)obj2;
        if(super.equals(auto2) &&
            this.numPassengers == auto2.getNumPassangers() &&
            this.isSUV == auto2.getIsSUV()){
                
            return true;
            }
        else{
            return false;
        }
    }
}