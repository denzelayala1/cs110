/*
   Denzel Ayala
   CS110

    This program defines the truck class which is an extention of the Vehicle class
    A truck is defined by the same fields as a vehicle but also by its capacity and 
    the number of axles it has
*/

public class Truck extends Vehicle{
    
    private int capacity, // An integer representing the number of tons the truck can carry
                numAxles; // an integer represnting the number of axles the truck contains

	private final int DEF_CAPACITY = 1; // the dedault number of tons a truck can carry
	private final int DEF_AXELS = 2; // the default number of axles a truck has


    //Constructors
    /**
     * Creates an instance of the truck object
     * @param own A Person obejct that represent the truck's owner
     * @param mk A string representing the make of the truck
     * @param mod A string representing the model of the truck
     * @param yr An integer representing the year of the truck
     * @param mile An integer representing the mileage of the truck
     * @param cap An integer representing the number of tons the truck can carry
     * @param numAx An integer representing the number of axles the truck has
     */
    public Truck(Person own, String mk, 
                 String mod, int yr, 
                 int mile, int cap,
                 int numAx){

        super(own, mk, mod, yr, mile); // call the vehicle constructor
        capacity = cap; 
        numAxles = numAx;
    }
    /**
     * A constructor that uses the default values for the truck's axles and capacity
     * @param name A String representing the owner's name
     * @param address A String representing the owner's address
     * @param phone A String representing the owner's phone number
     * @param mk A string representing the make of the truck
     * @param mod A string representing the model of the truck
     * @param yr An integer representing the year of the truck
     * @param mile An integer representing the mileage of the truck
     */
    public Truck(String name, String address,
                 String phone, String mk,
                 String mod, int yr,
                 int mile){
        // calling the vehicle constructor
        super(new Person(name, address, phone), // constructing a person object to represent the owner
              mk, mod, yr, mile);
        capacity = DEF_CAPACITY; 
        numAxles = DEF_AXELS;         
     }


     //Getters
     /**
      * Getter for the truck's carrying capacity 
      * @return An integer representing the number of tons the truck can carry
      */
     public int getCapacity(){
         return capacity;
     }

     /**
      * Getter for the number of axles the truck has
      * @return An integer representing the number axles the truck has
      */
     public int getNumAxles(){
         return numAxles;
     }

     
     //Setters
     /**
      * Setter for the carry weight of the truck in tons
      * @param capacity An integer that represent the number of tons the truck can carry
      */
     public void setCapacity(int capacity){
         this.capacity = capacity;
     }

     /**
      * Setter for the number of axles the truck has
      * @param numAxles An intger that represent the number of axles the truck has
      */
     public void setNumAxles(int numAxles){
         this.numAxles = numAxles;
     }


     //Additional methods
     /**
      *
      *@return A string that contains the truck's owner information, the make, 
      *        model, year, mileage, capacity, and number of axles the truck has
      */
     public String toString(){
         return super.toString() + " " + capacity + " ton " + numAxles + " axles";
     }

     /**
      * 
      *@return Returns True if the two objects share the same owner, 
               make, model, year, mileage, capacity, and number of axles
      */
     public boolean equals(Object obj2){
         Truck trk2 = (Truck)obj2;
         if(super.equals(trk2) &&
            this.capacity == trk2.getCapacity() &&
            this.numAxles == trk2.getNumAxles()){
                return true;
            }
         else{ 
             return false;
         } 

     }
}