/*
   Denzel Ayala
   CS110

    This program describes the Vehicle class
    a vehicle is defined by its owner which is a person object, its make, model, year and mileage
*/
import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

public class VehicleDriver{
    public static void main (String[] args) throws IOException{

        //Instantiate the file and check if it exists
        File fileVehicles = new File("vehicles.txt");
        if(fileVehicles.exists() == false){
            System.out.println("The file does not appear to be there. Please check input.");
            System.exit(0);
        }

        //open the file 
        Scanner scanFile = new Scanner(fileVehicles);

        //create an array list of vehicles
        ArrayList<Vehicle> vehicleList = new ArrayList<Vehicle>();

        //loop through the file reading each line
        while( scanFile.hasNextLine()){

            //store first line to check what vehicle type it is
            String check = scanFile.nextLine();

            // checks its its an Automobile
            if(check.equals("Automobile") ){

                //go through the file saving the values in the order they appear
                String name = scanFile.nextLine(),
                       address = scanFile.nextLine(),
                       phone = scanFile.nextLine(),
                       make = scanFile.nextLine(),
                       model = scanFile.nextLine();
                int    year = Integer.parseInt( scanFile.nextLine() ),
                       miles = Integer.parseInt( scanFile.nextLine() ),
                       passangers = Integer.parseInt( scanFile.nextLine() );
                boolean suv = Boolean.parseBoolean( scanFile.nextLine() );

                //construct an automobile using inputs from file
                Vehicle auto = new Automobile(name, address, phone, make, model, year, miles, passangers, suv);

                //adds automobile to the arrayList
                vehicleList.add(auto);
            }
            // checks its its a truck
            else if(check.equals("Truck") ){

                //go through the file saving the values in the order they appear
                String name = scanFile.nextLine(),
                       address = scanFile.nextLine(),
                       phone = scanFile.nextLine(),
                       make = scanFile.nextLine(),
                       model = scanFile.nextLine();
                int    year = Integer.parseInt( scanFile.nextLine() ),
                       miles = Integer.parseInt( scanFile.nextLine() ),
                       capacity = Integer.parseInt( scanFile.nextLine() ),
                       numAx = Integer.parseInt( scanFile.nextLine() );
                
                //construct a person object from input files
                Person own = new Person(name, address, phone);

                //construct a truck using inputs from file
                Vehicle trk = new Truck(own, make, model, year, miles, capacity, numAx);

                //adds truck to the arrayList
                vehicleList.add(trk);
            }
            // checks its its a taxi
            else if( check.equals("Taxi") ){
                String name = scanFile.nextLine(),
                       address = scanFile.nextLine(),
                       phone = scanFile.nextLine(),
                       make = scanFile.nextLine(),
                       model = scanFile.nextLine();
                int    year = Integer.parseInt( scanFile.nextLine() ),
                       miles = Integer.parseInt( scanFile.nextLine() ),
                       passangers = Integer.parseInt( scanFile.nextLine() );
                boolean suv = Boolean.parseBoolean( scanFile.nextLine() );
                String drName = scanFile.nextLine(),
                       drAdrs = scanFile.nextLine(),
                       drPhone = scanFile.nextLine(),
                       drID = scanFile.nextLine();
                
                //construct a taxi using inputs from file
                Vehicle tax = new Taxi(name, address, phone, make, model, year, 
                                        miles, passangers, suv,
                                        drName, drAdrs, drPhone, drID);

                //adds the taxi to the arrayList
                vehicleList.add(tax);
            }
            else{
                //if not a known vehicle closes the program
                System.out.println("This isn't a known vehicle type");
                System.exit(0);
            }        
        }

        // an integer that will hold the value for the oldest vehicle
        int lowestYear = vehicleList.get(0).getYear();

        // loops through the file looking for the oldest vehicle
        for(int i  = 0 ; i < vehicleList.size() ; i++){
            if(vehicleList.get(i).getYear() < lowestYear){
                lowestYear = vehicleList.get(i).getYear();
            }
        }

        //Creates an array list for vehicles to be sold
        ArrayList<Vehicle> toBeSold = new ArrayList<Vehicle>();

        //searches through the original arraylist to find all vehicles 
        //with the oldest year to put up for sale
        for( int i = 0; i < vehicleList.size(); i++){
            if( vehicleList.get(i).getYear() == lowestYear){
                toBeSold.add(vehicleList.get(i));
            }
        }
    
        //string to hold all the vehicles to be sold
        String stringToBeSold = "Vehicles to be sold:\n";

        //goes through the to be sold array list and formats them into strings
        for(int i = 0; i < toBeSold.size(); i++){

            if( toBeSold.get(i) instanceof Taxi){
                String addLine = "TAXI:  " + (i+1) + ": " + toBeSold.get(i).toString() + "\n\n";
                stringToBeSold += addLine ;
            }
            else if (toBeSold.get(i) instanceof Truck){
                stringToBeSold += "TRUCK:  " + (i+1) + ": " + toBeSold.get(i).toString() + "\n\n";
            } 
            else if( toBeSold.get(i) instanceof Automobile){
                stringToBeSold += "AUTOMOBILE:  " + (i+1) + ": " + toBeSold.get(i).toString() + "\n\n";
            }
            else{
                stringToBeSold += "This isn't a known vehicle type \n\n";
            }
        }

        //prints out the list of vehicles to be sold
        System.out.println(stringToBeSold);

        scanFile.close();


    }

}